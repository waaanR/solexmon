#pragma once
#include "Defines.h"
#include "I_Button.h"
#include "I_Display.h"
#include "mqtt.h"
#include "MessageDefinition.h"
#include "memory"
#include "queue"

/*                       MessageDialog behaviour:
 *
 *       What is sent to answer the stand depending on the mqtt mode received and the pilot action?
 *
 *   - (*) = notification (if the pilot's answer has been sent or not) displayed to the pilot
 *   - In all cases, when the YesBt or the NoBt is pressed, the symbol disappear (the display is reset)
 *   ----------------------------------------------------------------------------------------
 *   |          Pilot Action -->   |   YesBt    |   NoBt     |   None      |    What is     |
 *   |                             |            |            |             |   displayed?   |
 *   | Mode MQTT Received          |            |            |             |                |
 *   |-----------------------------|------------|------------|-------------|----------------|
 *   |            | want Yes/No    | Yes        | No         | 20s Timeout |                |
 *   | Request(*) |----------------|------------|------------|-------------| Message symbol |
 *   |            | want acquittal | ReceivedOk | ReceivedOk | 10s Timeout |                |
 *   |-----------------------------|------------|------------|-------------|----------------|
 *   | Yes                         | /          | /          | 10s /       | v/             |
 *   |-----------------------------|------------|------------|-------------|----------------|
 *   | No                          | /          | /          | 10s /       | X              |
 *   |-----------------------------|------------|------------|-------------|----------------|
 *   | ReceivedOk                  | /          | /          | 10s /       | v/             |
 *   |---------------------------------------------------------------------------------------
 *   | Timeout --> The same Message is re sent with the Request mode |
 *   -----------------------------------------------------------------
 *
*/

constexpr auto LoopFrequency=30;

struct DialogDisplay{
    DialogDisplay(I_Display& display, std::unique_ptr<I_Button> y ,std::unique_ptr<I_Button> n);

    enum class State{
        Yes,No,TimedOut,Inactive,DisplayingRequest,DisplayingIndication,DisplayingNotificationMsgSentOrNot
    };

    //! sets all the variables of the DialogDisplay to display the message
    void message(const Symbol& flashedSymbol, const Symbol& displayedSymbol, uint64_t timeout, uint64_t flashStartTime, State state);
    //! deals with the display time, the flashing and sets the state depending on the pilot's action
    void tick();

    inline State& status(){return state;}
    void reset(){state=State::Inactive;}

    I_Display& display;
private:
    std::unique_ptr<I_Button> YesBt,NoBt;
    bool flashBlackState=false;//!< true if the symbol is flashing and the display is on a reset phase (black screen)
    uint64_t frameDisplayedCount;//!< the number ot tick the last message has already been shown
    State state=State::Inactive;
    Symbol displayedSymbol, flashedSymbol;
    uint64_t timeout,flashStartTime;//!< timeout and blinkStartTime in seconds
};

struct MessageDialog
{
    // Constructor call in the "main.cpp":

    // For GPIO_Button:
    //         MessageDialog dialog = MessageDialog(displayM5,
    //                                              mqtt,
    //                                              std::unique_ptr<GPIO_Button>(new GPIO_Button(_gpioButton_)),
    //                                              std::unique_ptr<GPIO_Button>(new GPIO_Button(_gpioButton_)),
    //                                              std::unique_ptr<GPIO_Button>(new GPIO_Button(_gpioButton_)),
    //                                              std::unique_ptr<GPIO_Button>(new GPIO_Button(_gpioButton_)),
    //                                              std::unique_ptr<GPIO_Button>(new GPIO_Button(_gpioButton_)));

    // For Screen_Button:
    //         MessageDialog dialog = MessageDialog(displayM5,
    //                                              mqtt,
    //                                              std::unique_ptr<Screen_Button>(new Screen_Button(_idButton_)),
    //                                              std::unique_ptr<Screen_Button>(new Screen_Button(_idButton_)),
    //                                              std::unique_ptr<Screen_Button>(new Screen_Button(_idButton_)),
    //                                              std::unique_ptr<Screen_Button>(new Screen_Button(_idButton_)),
    //                                              std::unique_ptr<Screen_Button>(new Screen_Button(_idButton_)));

    //! tchPb = technicalProblem
    MessageDialog(I_Display &display,
                  MQTT &mqtt,
                  std::unique_ptr<I_Button> yesBt_,
                  std::unique_ptr<I_Button> noBt_,
                  std::unique_ptr<I_Button> wantToComeBackBt_,
                  std::unique_ptr<I_Button> tchPbBlockedBt_,
                  std::unique_ptr<I_Button> tchPbComingBackBt_);

    //! Message traitment, display and answer in function of the pilot action,
    //! return a bool if the display is used or not, to control the conflict between data and message displays
    bool tick();
    //! Add a message to the queue
    void handleMessage(MQTT::IntercomNetworkMessage &mqttMessage);
    void reset(int setCoolDown);

private:
    DialogDisplay dialogDisplay;
    MQTT& mqtt;
    std::queue<MQTT::IntercomNetworkMessage> queue;
    std::unique_ptr<I_Button> wantToComeBackBt,tchPbBlockedBt,tchPbComingBackBt;
    int coolDown=0;//!< timer that is incremented each tick, used before being able to send again
};
