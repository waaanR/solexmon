#pragma once

#include <VescUart.h>

struct I_Vesc{
    virtual ~I_Vesc(){}
    virtual decltype(VescUart::data)& currentData()=0;
    virtual bool queryData()=0;
};
