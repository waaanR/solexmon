﻿#include "mqtt.h"

#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <WiFi.h>
#include <time.h>


MQTT::MQTT(std::string solexId, std::string wifi_ssid, std::string wifi_pass)
    :SOLEX_ID(solexId)
    ,wifi_ssid(wifi_ssid)
    ,wifi_pass(wifi_pass)
    ,mqtt_stage4A("scalnsolex.scalian.com",8883,solexId,"solex-link-service","PJcg$Qt%Y5C$Mjo!47^q")
    ,mqtt_hackerman(IPAddress(93,22,23,35),1883,"solex","scalnsolex", "lovesos")
{
    TOPIC_MQTT_INPUT_SOLEX_MESSAGE = "output/" + SOLEX_ID + "/message";  //must be output/... (output for the server)
    TOPIC_MQTT_OUTPUT_SOLEX_MESSAGE = "input/" + SOLEX_ID + "/message";  //must be input/... (input for the server)
    TOPIC_MQTT_OUTPUT_SOLEX_DATA = "input/" + SOLEX_ID + "/data";  //must be input/... (input for the server)
    delay(1000);//waiting for Wifi hardware to be ready
    tryConnect(10000);
}

bool MQTT::ok(){return WiFi.status() == WL_CONNECTED && mqtt_stage4A.connected(); /*&& mqtt_hackerman.connected()*/}

bool MQTT::tryConnect(uint16_t timeout) {
    if(WiFi.status() != WL_CONNECTED) {
        WiFi.begin(wifi_ssid.c_str(), wifi_pass.c_str());
    }
    for (int i = 0; i < timeout; ++i) {
        if(WiFi.status() == WL_CONNECTED){
            //if(!mqtt_hackerman.connected()) mqtt_hackerman.connect();//deactivated for App testing (time reason)
            if(!mqtt_stage4A.connected()){
                mqtt_stage4A.connect();
                mqtt_stage4A.subscribe(TOPIC_MQTT_INPUT_SOLEX_MESSAGE.c_str());
            }
            if(!timeSet){
                configTime(0, 0, "pool.ntp.org");
                setenv("TZ","CET-1CEST,M3.5.0,M10.5.0/3",1);
                tzset();
                timeSet=true;
            }
            return true;
        }
        delay(1);
    }
    return false;
}

String get_timestamp() {
    tm ti;
    getLocalTime(&ti);
    char timestamp[16];
    sprintf(timestamp, "%02d%02d%02d%02d%02d%02d%03ld",
            ti.tm_year % 100, ti.tm_mon + 1, ti.tm_mday,
            ti.tm_hour, ti.tm_min, ti.tm_sec, long(0));
    return String(timestamp, 15);
}

bool MQTT::sendTelemetry(const Data& data){
    static StaticJsonDocument<400/*super good enought*/> json;

    if(tryConnect()){
        {
            String json_string;
            json.clear();
            json["type"] = 1;
            json["solexId"] = SOLEX_ID.c_str();
            json["timestamp"] = timeSet?get_timestamp():"000000000000000";
            json["latitude"] = data.latitude;
            json["longitude"] = data.longitude;
            json["tension"] = data.inpVoltage;
            json["temperature"] = data.tempMotor;
            serializeJson(json, json_string);
            mqtt_stage4A.publish((MQTT::TOPIC_MQTT_OUTPUT_SOLEX_DATA).c_str(), json_string.c_str());
        }
        {
            String json_string;
            json.clear();
            json["type"] = 1;
            json["solexId"] = SOLEX_ID.c_str();
            json["timestamp"] = timeSet?get_timestamp():"000000000000000";
            json["avgMotorCurrent"]=data.avgMotorCurrent;
            json["avgInputCurrent"]=data.avgInputCurrent;
            json["dutyCycleNow"]=data.dutyCycleNow;
            serializeJson(json, json_string);
            mqtt_stage4A.publish((MQTT::TOPIC_MQTT_OUTPUT_SOLEX_DATA + "2").c_str(), json_string.c_str());
        }
        {
            String json_string;
            json.clear();
            json["type"] = 1;
            json["solexId"] = SOLEX_ID.c_str();
            json["timestamp"] = timeSet?get_timestamp():"000000000000000";
            json["ampHours"]=data.ampHours;
            json["ampHoursCharged"]=data.ampHoursCharged;
            json["tempMosfet"]=data.tempMosfet;
            serializeJson(json, json_string);
            mqtt_stage4A.publish((MQTT::TOPIC_MQTT_OUTPUT_SOLEX_DATA + "3").c_str(), json_string.c_str());
        }
        {
            String json_string;
            json.clear();
            json["type"] = 1;
            json["solexId"] = SOLEX_ID.c_str();
            json["timestamp"] = timeSet?get_timestamp():"000000000000000";
            json["pidPos"]=data.pidPos;
            json["tachometer"]=data.tachometer;
            json["tachometerAbs"]=data.tachometerAbs;
            serializeJson(json, json_string);
            mqtt_stage4A.publish((MQTT::TOPIC_MQTT_OUTPUT_SOLEX_DATA + "4").c_str(), json_string.c_str());
        }
        {
            String json_string;
            json.clear();
            json["type"] = 1;
            json["solexId"] = SOLEX_ID.c_str();
            json["timestamp"] = timeSet?get_timestamp():"000000000000000";
            json["rpm"]=data.rpm;
            json["wattHours"]=data.wattHours;
            json["wattHoursCharged"]=data.wattHoursCharged;
            serializeJson(json, json_string);
            mqtt_stage4A.publish((MQTT::TOPIC_MQTT_OUTPUT_SOLEX_DATA + "5").c_str(), json_string.c_str());
        }

        mqtt_stage4A.loop();
        //mqtt_hackerman.loop();
        return true;
    }
    return false;
}

std::string MQTT::getDate(){
    if(!timeSet) return "00/00-00:00:00";

    tm ti;
    getLocalTime(&ti);
    char timestamp[16];
    sprintf(timestamp, "%02d/%02d-%02d:%02d:%02d",
            ti.tm_mday, ti.tm_mon + 1, ti.tm_hour, ti.tm_min, ti.tm_sec);
    return std::string(timestamp);
}

MQTT::MQTT_Client::MQTT_Client(IPAddress ip, uint16_t port, std::string id, std::string user, std::string pass)
    :PubSubClient(wifi_client),ip(ip),port(port),id(id),username(user),password(pass)
{}

MQTT::MQTT_Client::MQTT_Client(std::string domain, uint16_t port, std::string id, std::string user, std::string pass)
    :PubSubClient(wifi_client),domain(domain),port(port),id(id),username(user),password(pass)
{}

void MQTT::MQTT_Client::connect(){
    if(domain.empty()){
        PubSubClient::setServer(ip,port);
    }else{
        PubSubClient::setServer(domain.c_str(),port);
    }
    PubSubClient::connect(id.c_str(),username.c_str(),password.c_str());
}

const std::map<std::string,MQTT::IntercomNetworkMessage::Mode> MQTT::IntercomNetworkMessage::modes = {
    {"Request",Mode::Request},
    {"Yes",Mode::Yes},
    {"No",Mode::No},
    {"Timeout",Mode::Timeout},
    {"ReceivedOk",Mode::ReceivedOk}
};

const std::string MQTT::IntercomNetworkMessage::modeToString(){
    for(std::pair<std::string,MQTT::IntercomNetworkMessage::Mode> m:modes){
        if(modality==m.second){
            return m.first;
        }
    }
    return "null";
}

const MQTT::IntercomNetworkMessage::Mode MQTT::IntercomNetworkMessage::modeFromString(std::string mode){
    std::map<std::string,MQTT::IntercomNetworkMessage::Mode>::const_iterator it = modes.find(mode);
    if(it!=modes.end()){
        return it->second;
    }else{
        return Mode::MalFormed;
    }
}

const std::string MQTT::IntercomNetworkMessage::toJson(bool timeset){
    String timestamp = timeset?get_timestamp():"000000000000000";
    return "{\"type\":\"0\",\"solexId\":\""+solexId+"\",\"timestamp\":\""+timestamp.c_str()+"\",\"messageId\":\""+messageId+"\",\"modality\":\""+modeToString()+"\"}";
}

const MQTT::IntercomNetworkMessage MQTT::IntercomNetworkMessage::fromJsonBuffer(uint8_t *buffer, unsigned int bufferSize) {
    std::string payload = "";
    for(int i=0;i<bufferSize;i++){
        payload = payload + ((char) buffer[i]);
    }
    static StaticJsonDocument<400/*super good enought*/> json;
    deserializeJson(json, payload);
    return MQTT::IntercomNetworkMessage(json["solexId"],json["messageId"],modeFromString(json["modality"]));
}

bool MQTT::sendMessage(MQTT::IntercomNetworkMessage& msg){
    if(tryConnect()){
        if(!strcmp(msg.solexId.c_str(), this->SOLEX_ID.c_str())){
            if(msg.modality!=MQTT::IntercomNetworkMessage::Mode::MalFormed){
                mqtt_stage4A.publish(TOPIC_MQTT_OUTPUT_SOLEX_MESSAGE.c_str(), (msg.toJson(timeSet)).c_str());
                mqtt_stage4A.loop();
                return true;
            }
        }
    }
    return false;
}
