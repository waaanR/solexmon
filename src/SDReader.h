#pragma once
#include "Defines.h"
#include "DisplayLED.h"
#include "DisplayM5.h"
#include "I_Display.h"
#include <SD.h>
#include <ArduinoJson.h>


struct SDReader
{
#ifndef MOCK_SD
    SDReader();

    void getConfig();

    //!setConfig used against '= overloaded' error
    void setConfig(uint8_t m5Version,
                   bool mock_UART,
                   bool mock_GPS,
                   std::string buttons,
                   std::string solexId,
                   std::string messageDisplay,
                   int ledBrightnessIfLedUsed,
                   std::string wifi_ssid,
                   std::string wifi_pass,
                   int yesBt,
                   int noBt,
                   int wantToComeBackBt,
                   int blockedBt,
                   int tchPbComingBackBt,
                   double lowThresholdVoltage,
                   double veryLowThresholdVoltage,
                   double highThresholdTempMotor,
                   double veryHighThresholdTempMotor,
                   double highThresholdTempMosfet,
                   double veryHighThresholdTempMosfet);

#else
    SDReader();

#endif

    bool configOk;

    uint8_t m5Version;//!<1 or 2
    bool mock_UART;
    bool mock_GPS;
    std::string buttons;//!<Touch or Press
    std::string solexId;
    std::string messageDisplay;//!<DisplayM5 or DisplayLED
    int ledBrightnessIfLedUsed;
    std::string wifi_ssid;
    std::string wifi_pass;

    //buttons gpio for 'Press buttons' config
    int yesBt;
    int noBt;
    int wantToComeBackBt;
    int blockedBt;
    int tchPbComingBackBt;

    //thresholds for data colors
    double lowThresholdVoltage;
    double veryLowThresholdVoltage;
    double highThresholdTempMotor;
    double veryHighThresholdTempMotor;
    double highThresholdTempMosfet;
    double veryHighThresholdTempMosfet;
};
