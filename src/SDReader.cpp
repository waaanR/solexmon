#include "SDReader.h"

#ifndef MOCK_SD
SDReader::SDReader(){
    bool sdInited = false;
    if(!sdInited){
        while (!SD.begin(GPIO_NUM_4, SPI, 25000000)){
            delay(10);
        }
        sdInited=true;
    }
    this->configOk = false;
    getConfig();
}

void SDReader::getConfig() {
    File file = SD.open("/Config.json");
    if(!file){
        this->configOk = false;
        return;
    }

    std::string payload = "";
    while(file.available()){
        int ch = file.read();
        payload = payload + (char) ch;
    }
    static StaticJsonDocument<1200/*super good enought*/> json;
    deserializeJson(json, payload);

    setConfig(
        json["m5Version"],
        json["mock_UART"],
        json["mock_GPS"],
        json["buttons"],
        json["solexId"],
        json["messageDisplay"],
        json["ledBrightnessIfLedUsed"],
        json["wifi_ssid"],
        json["wifi_pass"],
        json["yesBt"],
        json["noBt"],
        json["wantToComeBackBt"],
        json["blockedBt"],
        json["tchPbComingBackBt"],
        json["lowThresholdVoltage"],
        json["veryLowThresholdVoltage"],
        json["highThresholdTempMotor"],
        json["veryHighThresholdTempMotor"],
        json["highThresholdTempMosfet"],
        json["veryHighThresholdTempMosfet"]);

    this->configOk = true;
}

void SDReader::setConfig(uint8_t m5Version,
                         bool mock_UART,
                         bool mock_GPS,
                         std::string buttons,
                         std::string solexId,
                         std::string messageDisplay,
                         int ledBrightnessIfLedUsed,
                         std::string wifi_ssid,
                         std::string wifi_pass,
                         int yesBt,
                         int noBt,
                         int wantToComeBackBt,
                         int blockedBt,
                         int tchPbComingBackBt,
                         double lowThresholdVoltage,
                         double veryLowThresholdVoltage,
                         double highThresholdTempMotor,
                         double veryHighThresholdTempMotor,
                         double highThresholdTempMosfet,
                         double veryHighThresholdTempMosfet){
    this->m5Version = m5Version;
    this->mock_UART = mock_UART;
    this->mock_GPS = mock_GPS;
    this->buttons = buttons;
    this->solexId = solexId;
    this->messageDisplay = messageDisplay;
    this->ledBrightnessIfLedUsed = ledBrightnessIfLedUsed;
    this->wifi_ssid = wifi_ssid;
    this->wifi_pass = wifi_pass;
    this->yesBt = yesBt;
    this->noBt = noBt;
    this->wantToComeBackBt = wantToComeBackBt;
    this->blockedBt = blockedBt;
    this->tchPbComingBackBt = tchPbComingBackBt;
    this->lowThresholdVoltage = lowThresholdVoltage;
    this->veryLowThresholdVoltage = veryLowThresholdVoltage;
    this->highThresholdTempMotor = highThresholdTempMotor;
    this->veryHighThresholdTempMotor = veryHighThresholdTempMotor;
    this->highThresholdTempMosfet = highThresholdTempMosfet;
    this->veryHighThresholdTempMosfet = veryHighThresholdTempMosfet;
}
#else
SDReader::SDReader(){
    m5Version = 2;
    mock_UART = true;
    mock_GPS = true;
    buttons = "Touch";
    solexId = "135f33f0-1791-4aae-ac05-f11510e57ca6";
    messageDisplay = "DisplayM5";
    ledBrightnessIfLedUsed = 5;
    wifi_ssid = "ASUS_4980";//"AndroidAP_5731";
    wifi_pass = "belkinbelkin";//"jcjaz434i3r55jf";
    yesBt = 27;
    noBt = 27;
    wantToComeBackBt = 27;
    blockedBt = 27;
    tchPbComingBackBt = 27;
    lowThresholdVoltage = 52.5;
    veryLowThresholdVoltage = 50.5;
    highThresholdTempMotor = 52.5;
    veryHighThresholdTempMotor = 50.5;
    highThresholdTempMosfet = 52.5;
    veryHighThresholdTempMosfet = 50.5;
    configOk = true;
}
#endif
