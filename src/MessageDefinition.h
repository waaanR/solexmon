#pragma once
#include "mqtt.h"
#include "Symbol.h"

constexpr auto changePilotID = "25";
constexpr auto returnToStandID = "13";
constexpr auto fasterID = "17";
constexpr auto lowerID = "21";
constexpr auto iWantToComeBackID = "1";
constexpr auto technicalPb_BlockedID = "5";//!<technicalPb=technicalProblem
constexpr auto technicalPb_ComingBackID=  "9";//!<technicalPb=technicalProblem

struct MessageDefinition{
    MessageDefinition(MQTT::IntercomNetworkMessage::MessageID selfId,std::vector<MQTT::IntercomNetworkMessage::Mode> possibleAnswers,Symbol symbol):
        selfId(selfId),possibleAnswers(possibleAnswers),symbol(symbol){}

    static const MessageDefinition changePilot;
    static const MessageDefinition returnToStand;
    static const MessageDefinition faster;
    static const MessageDefinition lower;
    static const MessageDefinition iWantToComeBack;
    static const MessageDefinition technicalPb_Blocked;//!<technicalPb=technicalProblem
    static const MessageDefinition technicalPb_ComingBack;//!<technicalPb=technicalProblem
    
    static const std::map<MQTT::IntercomNetworkMessage::MessageID,MessageDefinition> database;//!< link a message and its Id

    //! true if the message has 'mode' in its possible responses
    const bool acceptedAnswerIfRequest(MQTT::IntercomNetworkMessage::Mode mode);
    
    const MQTT::IntercomNetworkMessage::MessageID selfId;
    const std::vector<MQTT::IntercomNetworkMessage::Mode> possibleAnswers;
    const Symbol symbol;
};
