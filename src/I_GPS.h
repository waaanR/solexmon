#pragma once

#include <vector>
#include <TinyGPS++.h>

struct I_GPS{
    struct Pos{
        double lat=0,lon=0;
    };

    virtual ~I_GPS(){}
    virtual void synch()=0;
    virtual uint32_t satCount()=0;
    virtual Pos& currentPos()=0;
};
