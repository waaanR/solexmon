#pragma once
#include <memory>
#include <map>
#include <WString.h>
#include <PubSubClient.h>
#include <WiFiClient.h>
#include <array>
#include <vector>
#include "SDReader.h"


struct MQTT{
    MQTT(std::string solexId, std::string wifi_ssid, std::string wifi_pass);
    bool ok();
    std::string getDate();

    struct Data {
        float latitude;
        float longitude;
        float avgMotorCurrent;
        float avgInputCurrent;
        float dutyCycleNow;
        float rpm;
        float inpVoltage;
        float ampHours;
        float ampHoursCharged;
        float wattHours;
        float wattHoursCharged;
        float tempMosfet;
        float tempMotor;
        float pidPos;
        long tachometer;
        long tachometerAbs;
    };
    bool sendTelemetry(const Data& data);

    struct IntercomNetworkMessage{
        using MessageID = std::string;
        using SolexID = std::string;
        enum class Mode{
            Request,
            Yes,//!< Request answer, positive answer
            No,//!< Request answer, negative answer
            Timeout,//!< Request answer, message arrived but the driver did not answer
            ReceivedOk,//!< Request answer, message arrived and noticed by the driver
            MalFormed//!< State against boundaries
        };

        IntercomNetworkMessage(SolexID agentId,MessageID id,Mode modality):
            solexId(agentId),messageId(id),modality(modality){}

        const std::string toJson(bool timeset);
        static const IntercomNetworkMessage fromJsonBuffer(uint8_t* buffer, unsigned int bufferSize);

        static const std::map<std::string,Mode> modes;//!< link a mode and its string
        const std::string modeToString();
        static const Mode modeFromString(std::string mode);

        SolexID solexId;//!< solexId
        MessageID messageId;//!< same as request for a responce
        Mode modality;//!< mode, request or answer to a request (not request)
    };
    bool sendMessage(IntercomNetworkMessage& msg);

    struct MQTT_Client:PubSubClient{
        MQTT_Client(IPAddress ip, uint16_t port,std::string id, std::string user, std::string pass);
        MQTT_Client(std::string domain, uint16_t port,std::string id, std::string user, std::string pass);
        void connect();
    private:
        WiFiClient wifi_client;
        std::string id, username, password, domain;
        uint16_t port;
        IPAddress ip;
    };
    MQTT_Client mqtt_stage4A;

    std::string SOLEX_ID;
    std::string TOPIC_MQTT_INPUT_SOLEX_MESSAGE;
    std::string TOPIC_MQTT_OUTPUT_SOLEX_MESSAGE;
    std::string TOPIC_MQTT_OUTPUT_SOLEX_DATA;

private:
    bool tryConnect(uint16_t timeout=1/*ms*/);

    // Wifi related
    std::string wifi_ssid; //"ASUS_4980";//"AndroidAP_5731";
    std::string wifi_pass; //"belkinbelkin";//"jcjaz434i3r55jf";

    bool timeSet=false;

    MQTT_Client mqtt_hackerman;
};
