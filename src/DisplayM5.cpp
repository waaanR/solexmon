#include "DisplayM5.h"

DisplayM5::DisplayM5(double lowThresholdVoltage,
                     double veryLowThresholdVoltage,
                     double highThresholdTempMotor,
                     double veryHighThresholdTempMotor,
                     double highThresholdTempMosfet,
                     double veryHighThresholdTempMosfet):
    lowThresholdVoltage(lowThresholdVoltage),
    veryLowThresholdVoltage(veryLowThresholdVoltage),
    highThresholdTempMotor(highThresholdTempMotor),
    veryHighThresholdTempMotor(veryHighThresholdTempMotor),
    highThresholdTempMosfet(highThresholdTempMosfet),
    veryHighThresholdTempMosfet(veryHighThresholdTempMosfet){
    begin();
    setRotation(2);
}

void DisplayM5::drawMessage(Symbol symbol){
    if (!displayBusy()){
        startWrite();{

            setColor(TFT_BLACK);
            fillRect(0,320*bigOffset,240,320*(1-bigOffset));

            auto drawPixelFull=[&](float x,float y){
                for(int k=0;k<30;k++){
                    for(int l=0;l<30;l++){
                        drawPixel(30*x+k,320*bigOffset+30*y+l);
                    }
                }
            };
            auto drawPixelBottomLeftCorner=[&](float x,float y){
                for(int k=0;k<30;k++){
                    for(int l=0;l<30;l++){
                        if(l>=k){
                            drawPixel(30*x+k,320*bigOffset+30*y+l);
                        }
                    }
                }
            };
            auto drawPixelTopLeftCorner=[&](float x,float y){
                for(int k=0;k<30;k++){
                    for(int l=0;l<30;l++){
                        if(l+k<30){
                            drawPixel(30*x+k,320*bigOffset+30*y+l);
                        }
                    }
                }
            };
            auto drawPixelTopRightCorner=[&](float x,float y){
                for(int k=0;k<30;k++){
                    for(int l=0;l<30;l++){
                        if(l<=k){
                            drawPixel(30*x+k,320*bigOffset+30*y+l);
                        }
                    }
                }
            };
            auto drawPixelBottomRightCorner=[&](float x,float y){
                for(int k=0;k<30;k++){
                    for(int l=0;l<30;l++){
                        if(l+k>=30){
                            drawPixel(30*x+k,320*bigOffset+30*y+l);
                        }
                    }
                }
            };

            switch(symbol.color){
            case Symbol::Color::None:
                setColor(TFT_BLACK);
                break;
            case Symbol::Color::Purple:
                setColor(TFT_PURPLE);
                break;
            case Symbol::Color::Blue:
                setColor(TFT_BLUE);
                break;
            case Symbol::Color::Cyan:
                setColor(TFT_CYAN);
                break;
            case Symbol::Color::Green:
                setColor(TFT_GREEN);
                break;
            case Symbol::Color::Yellow:
                setColor(TFT_YELLOW);
                break;
            case Symbol::Color::Orange:
                setColor(TFT_ORANGE);
                break;
            case Symbol::Color::Red:
                setColor(TFT_RED);
                break;
            case Symbol::Color::White:
                setColor(TFT_WHITE);
                break;
            default:
                drawERROR();
                return;
            }

            //draw the symbol in parameter
            for(int i=0;i<8;i++){
                for(int j=0;j<8;j++){
                    switch(symbol.bitmap.lines[i][j]){
                    case Symbol::PixelState::EMP:
                        break;
                    case Symbol::PixelState::FUL:
                        drawPixelFull(j,i);
                        break;
                    case Symbol::PixelState::BLC:
                        drawPixelBottomLeftCorner(j,i);
                        break;
                    case Symbol::PixelState::TLC:
                        drawPixelTopLeftCorner(j,i);
                        break;
                    case Symbol::PixelState::TRC:
                        drawPixelTopRightCorner(j,i);
                        break;
                    case Symbol::PixelState::BRC:
                        drawPixelBottomRightCorner(j,i);
                        break;
                    }
                }
            }
        }endWrite();
    }
}

void DisplayM5::drawERROR(){
    if (!displayBusy()){
        for (auto& color : {TFT_RED,TFT_WHITE}) {
            startWrite();{
                fillScreen(color);
                setTextColor(TFT_BLACK);
                setTextDatum(textdatum_t::top_centre);
                setFont(&fonts::DejaVu40);
                drawString("UART",240/2,320/3);
                drawString("ERROR",240/2,2*320/3);
            }endWrite();
        }
    }
}

void DisplayM5::drawData(decltype(VescUart::data)& data, bool mqttOK, std::string date, uint32_t satCount){
    if (!displayBusy()){
        startWrite();{
            fillScreen(TFT_BLACK);
            setTextColor(TFT_WHITE);

            auto draw=[&](std::string s,float x,float y){
                drawString(s.c_str(),240*x,320*y);
            };

            setFont(&fonts::DejaVu12);
            setTextDatum(textdatum_t::top_left);
            draw(std::string("MQTT: ")+(mqttOK?"OK":"NOK"),0,0);
            draw(date,0.4,0);
            draw("sat:"+std::to_string(satCount),0,0.05);

            auto drawBigLine=[&](std::string title,std::string val,float y){
                setFont(&fonts::DejaVu24);
                setTextDatum(textdatum_t::top_left);
                draw(title,0,y);
                setFont(&fonts::DejaVu72);
                setTextDatum(textdatum_t::top_centre);
                draw(val,0.5,y+0.08);
            };

            if(data.inpVoltage<this->lowThresholdVoltage)setTextColor(TFT_ORANGE);
            if(data.inpVoltage<this->veryLowThresholdVoltage)setTextColor(TFT_RED);
            drawBigLine("Voltage",std::to_string(data.inpVoltage).substr(0,5),bigRatio*0.00+bigOffset);

            setTextColor(TFT_WHITE);
            if(data.tempMotor>this->highThresholdTempMotor)setTextColor(TFT_ORANGE);
            if(data.tempMotor>this->veryHighThresholdTempMotor)setTextColor(TFT_RED);
            drawBigLine("T mot",std::to_string(data.tempMotor).substr(0,5) ,bigRatio*0.33+bigOffset);

            setTextColor(TFT_WHITE);
            if(data.tempMosfet>this->highThresholdTempMosfet)setTextColor(TFT_ORANGE);
            if(data.tempMosfet>this->veryHighThresholdTempMosfet)setTextColor(TFT_RED);
            drawBigLine("T fet",std::to_string(data.tempMosfet).substr(0,5),bigRatio*0.66+bigOffset);

        }endWrite();
    }
}

void DisplayM5::info(std::string s){
    constexpr auto S=22;
    if(s.length()>S){
        info(s.substr(0,S));
        info(s.substr(S,s.length()));
        return;
    }

    if(infoBuffer.size()>12){
        infoBuffer.pop_front();
    }
    infoBuffer.push_back(s);
    if (!displayBusy()){
        startWrite();{
            fillScreen(TFT_BLACK);
            setTextColor(TFT_WHITE);
            setTextDatum(textdatum_t::top_left);
            setFont(&fonts::DejaVu18);

            uint8_t lineIndex=0;
            for (auto& infoLine : infoBuffer) {
                drawString(infoLine.c_str(),0,24*lineIndex++);
            }
        }endWrite();
    }
}
