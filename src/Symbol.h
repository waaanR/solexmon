#pragma once

#include <array>
#include <map>

struct Symbol{
    enum class Color{
        None,
        Purple,
        Blue,
        Cyan,
        Green,
        Yellow,
        Orange,
        Red,
        White
    };

    //! EMP=empty, FUL=full, BLC=bottomLeftCorner, TLC=topLeftCorner, TRC=topRightCorner, BRC=bottomRightCorner
    enum class PixelState{
        EMP,
        FUL,
        BLC,
        TLC,
        TRC,
        BRC
    };

    struct Bitmap{
        using Line = std::array<PixelState,8>;//!< pixel states from left to right
        std::array<Line,8> lines;//!< lines from top to bottom
    };

    Symbol(std::array<PixelState,64>,Color);

    static const std::map<std::string,Color> colors;//!< Table to get the Color from a String

    static const Symbol reset_Symbol;//!< Symbol used to reset the display
    static const Symbol changePilot_Symbol;//!< Symbol used to ask the pilot to change next time
    static const Symbol returnToStand_Symbol;//!< Symbol used to ask the pilot to return to stand
    static const Symbol faster_Symbol;//!< Symbol used to ask the pilot to accelerate
    static const Symbol lower_Symbol;//!< Symbol used to ask the pilot to slow down
    static const Symbol iWantToComeBack_Symbol;//!< Symbol used to display the answer of the stand for the 'iWantToComeBack' request
    static const Symbol technicalPb_Blocked_Symbol;//!< Symbol used to display the answer of the stand for the 'technicalPb_Blocked' request
    static const Symbol technicalPb_ComingBack_Symbol;//!< Symbol used to display the answer of the stand for the 'technicalPb_ComingBack' request
    static const Symbol yes_Symbol;//!< Symbol used to display the YES answer of stand
    static const Symbol no_Symbol;//!< Symbol used to display the NO answer of stand
    static const Symbol send_Message_Ok_Symbol;//!< Symbol used to confirm that a message has been sent from the pilot to the stand
    static const Symbol send_Message_Not_Ok_Symbol;//!< Symbol used when the pilot wanted to send a message to the stand but it has failed
    static const Symbol error_Symbol;//!< Symbol used to display an error on the DisplayLED


    Color color;
    Bitmap bitmap;
};
