#pragma once
#include "M5GFX.h"
#include "I_Display.h"
#include "Vuart.h"
#include <list>

//! drawMessage from DisplayM5 deals with all the possible states of a symbol pixel state
struct DisplayM5: public I_Display,M5GFX{
    DisplayM5(double lowThresholdVoltage,
              double veryLowThresholdVoltage,
              double highThresholdTempMotor,
              double veryHighThresholdTempMotor,
              double highThresholdTempMosfet,
              double veryHighThresholdTempMosfet);

    void drawMessage(Symbol symbol) override;
    void drawERROR() override;
    void drawData(decltype(VescUart::data)& data, bool mqttOK, std::string date, uint32_t satCount);
    void info(std::string s);
private:
    const double bigRatio=0.90;
    const double bigOffset=0.12;
    std::list<std::string> infoBuffer;

    double lowThresholdVoltage;
    double veryLowThresholdVoltage;
    double highThresholdTempMotor;
    double veryHighThresholdTempMotor;
    double highThresholdTempMosfet;
    double veryHighThresholdTempMosfet;
};
