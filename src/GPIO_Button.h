#pragma once
#include "I_Button.h"
#include "sys/_stdint.h"
#include "Arduino.h"

//! a button in INPUT PULLUP pinmode that is linked to a gpio on a microcontroller
struct GPIO_Button: public I_Button{
    GPIO_Button(uint8_t gpio);
    bool isPressed() override;//!< true if the button is pressed, false if it is released
private:
    uint8_t gpio;
};
