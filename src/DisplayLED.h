#pragma once
#include "I_Display.h"
#include "FastLED.h"

constexpr auto MATRIX_HEIGHT = 8;
constexpr auto MATRIX_WIDTH = 8;

//! drawMessage from DisplayLED only deals with a pixel state empty or not (if a pixel state is not empty, therefore it is full)
struct DisplayLED: public I_Display{
    DisplayLED(int ledBrightness);

    void drawMessage(Symbol symbol) override;
    void drawERROR() override;

    using PixelColor = CRGB;
    struct Matrix{
        using LineColor = PixelColor[MATRIX_WIDTH];//!< pixel color from left to right
        LineColor colors[MATRIX_HEIGHT];//!< lines color from top to bottom
    };
    void resetMatrix();
    void setMatrix(Symbol symbol);
    void display();
private:
    int LED_BRIGHTNESS;
    Matrix matrix;
};
