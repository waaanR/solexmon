#pragma once
#include "Symbol.h"

struct I_Display{
    virtual ~I_Display(){}//destructor
    virtual void drawMessage(Symbol symbol)=0;
    virtual void drawERROR()=0;
};
