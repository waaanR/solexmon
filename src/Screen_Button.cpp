#include "Screen_Button.h"
#include "M5Core2.h"

Screen_Button::Screen_Button(uint8_t idButton){
    switch(idButton){
    case(1):
        button = new Button(4, 240, 100, 40);
        break;
    case(2):
        button = new Button(110, 240, 100, 40);
        break;
    case(3):
        button = new Button(216, 240, 101, 40);
        break;
    case(4):
        button = new Button(0, 240, 50, 40);
        break;
    case(5):
        button = new Button(56, 240, 101, 40);
        break;
    case(6):
        button = new Button(163, 240, 101, 40);
        break;
    case(7):
        button = new Button(270, 240, 50, 40);
        break;
    case(8):
        button = new Button(0, 0, 320, 220);
        break;
    default:
        button = new Button(0, 0, 320, 240);
        break;
    }
}

Screen_Button::~Screen_Button(){
    delete button;
}

bool Screen_Button::isPressed(){
    M5.update();
    return button->isPressed();
}

void Screen_Button::initializeHardware(){
    M5.begin();// used to init the M5Core2 part, useful to use the Screen_Buttons
}
