#include "Symbol.h"

Symbol::Symbol(std::array<PixelState,64> bitearray, Color color):color(color){
    bitmap.lines=std::array<Bitmap::Line,8>();
    for(int i=0;i<8;i++){
        for(int j=0;j<8;j++){
            bitmap.lines[i][j] = bitearray[8*i+j];
        }
    }
}

const std::map<std::string,Symbol::Color> Symbol::colors = {
    {"None",Color::None},
    {"Purple",Color::Purple},
    {"Blue",Color::Blue},
    {"Cyan",Color::Cyan},
    {"Green",Color::Green},
    {"Yellow",Color::Yellow},
    {"Orange",Color::Orange},
    {"Red",Color::Red},
    {"White",Color::White}
};

const Symbol Symbol::reset_Symbol = Symbol(std::array<PixelState,64>(
                                               {PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL}), Color::None);

const Symbol Symbol::changePilot_Symbol = Symbol(std::array<PixelState,64>(
                                                     {PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::EMP,
                                                      PixelState::EMP, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::BLC, PixelState::EMP,
                                                      PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP,
                                                      PixelState::EMP, PixelState::BRC, PixelState::BLC, PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::FUL, PixelState::TLC,
                                                      PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::TRC, PixelState::TLC, PixelState::EMP,
                                                      PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                                      PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::EMP,
                                                      PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::TLC, PixelState::EMP}), Color::Red);

const Symbol Symbol::returnToStand_Symbol = Symbol(std::array<PixelState,64>(
                                                       {PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                                        PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::EMP,
                                                        PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP,
                                                        PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::BLC,
                                                        PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::EMP,
                                                        PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP,
                                                        PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP,
                                                        PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP}), Color::Red);

const Symbol Symbol::faster_Symbol = Symbol(std::array<PixelState,64>(
                                                {PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                                 PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::TLC, PixelState::TRC, PixelState::BLC, PixelState::EMP, PixelState::EMP,
                                                 PixelState::EMP, PixelState::BRC, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::BLC, PixelState::EMP,
                                                 PixelState::BRC, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::BLC,
                                                 PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                                 PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::TLC, PixelState::TRC, PixelState::BLC, PixelState::EMP, PixelState::EMP,
                                                 PixelState::EMP, PixelState::BRC, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::BLC, PixelState::EMP,
                                                 PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP}), Color::Green);

const Symbol Symbol::lower_Symbol = Symbol(std::array<PixelState,64>(
                                               {PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                                PixelState::EMP, PixelState::TRC, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::TLC, PixelState::EMP,
                                                PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::BLC, PixelState::BRC, PixelState::TLC, PixelState::EMP, PixelState::EMP,
                                                PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                                PixelState::TRC, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::TLC,
                                                PixelState::EMP, PixelState::TRC, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::TLC, PixelState::EMP,
                                                PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::BLC, PixelState::BRC, PixelState::TLC, PixelState::EMP, PixelState::EMP,
                                                PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::EMP}), Color::Orange);

const Symbol Symbol::yes_Symbol = Symbol(std::array<PixelState,64>(
                                             {PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                              PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::BLC,
                                              PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::TLC,
                                              PixelState::BRC, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::TLC, PixelState::EMP,
                                              PixelState::TRC, PixelState::FUL, PixelState::BLC, PixelState::BRC, PixelState::FUL, PixelState::TLC, PixelState::EMP, PixelState::EMP,
                                              PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::FUL, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                              PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                              PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP}), Color::Green);

const Symbol Symbol::no_Symbol = Symbol(std::array<PixelState,64>(
                                            {PixelState::BRC, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::BLC,
                                             PixelState::TRC, PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::TLC,
                                             PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::BLC, PixelState::BRC, PixelState::FUL, PixelState::TLC, PixelState::EMP,
                                             PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::FUL, PixelState::TLC, PixelState::EMP, PixelState::EMP,
                                             PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::EMP,
                                             PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::TLC, PixelState::TRC, PixelState::FUL, PixelState::BLC, PixelState::EMP,
                                             PixelState::BRC, PixelState::FUL, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::BLC,
                                             PixelState::TRC, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::TRC, PixelState::TLC}), Color::Red);

const Symbol Symbol::send_Message_Ok_Symbol = Symbol(std::array<PixelState,64>(
                                                         {PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                                             PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::TLC, PixelState::EMP,
                                                             PixelState::BLC, PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::FUL, PixelState::TLC, PixelState::EMP, PixelState::BRC,
                                                             PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::TRC, PixelState::TLC, PixelState::EMP, PixelState::BRC, PixelState::FUL,
                                                             PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::FUL,
                                                             PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                             PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                             PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP,}), Color::Green);

const Symbol Symbol::send_Message_Not_Ok_Symbol = Symbol(std::array<PixelState,64>(
                                                             {PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                                                 PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::TLC, PixelState::EMP,
                                                                 PixelState::BLC, PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::FUL, PixelState::TLC, PixelState::EMP, PixelState::BRC,
                                                                 PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::TRC, PixelState::TLC, PixelState::EMP, PixelState::BRC, PixelState::FUL,
                                                                 PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::FUL,
                                                                 PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                                 PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                                 PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::EMP,}), Color::Red);

const Symbol Symbol::error_Symbol = Symbol(std::array<PixelState,64>(
                                               {PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL,
                                                PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL}), Color::Red);

const Symbol Symbol::iWantToComeBack_Symbol = Symbol(std::array<PixelState,64>(
                                                         {PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                                          PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::EMP,
                                                          PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP,
                                                          PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::BLC,
                                                          PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::EMP,
                                                          PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP,
                                                          PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP,
                                                          PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP}), Color::Cyan);

const Symbol Symbol::technicalPb_Blocked_Symbol = Symbol(std::array<PixelState,64>(
                                                             {PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP,
                                                              PixelState::BRC, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::BLC,
                                                              PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::TLC, PixelState::FUL,
                                                              PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::TLC, PixelState::EMP, PixelState::FUL,
                                                              PixelState::FUL, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::FUL,
                                                              PixelState::FUL, PixelState::BRC, PixelState::FUL, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::FUL,
                                                              PixelState::TRC, PixelState::FUL, PixelState::TLC, PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::TLC,
                                                              PixelState::EMP, PixelState::TRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::TLC, PixelState::EMP}), Color::Orange);

const Symbol Symbol::technicalPb_ComingBack_Symbol = Symbol(std::array<PixelState,64>(
                                                                {PixelState::EMP, PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::BLC, PixelState::EMP, PixelState::EMP, PixelState::EMP,
                                                                 PixelState::EMP, PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP, PixelState::EMP,
                                                                 PixelState::EMP, PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::BLC, PixelState::EMP,
                                                                 PixelState::BRC, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::BLC,
                                                                 PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::FUL, PixelState::EMP,
                                                                 PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP,
                                                                 PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP,
                                                                 PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP, PixelState::EMP, PixelState::FUL, PixelState::FUL, PixelState::EMP}), Color::Orange);
