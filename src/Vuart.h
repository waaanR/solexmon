#pragma once

#include "I_Vuart.h"

struct Vesc:private VescUart, I_Vesc{
    Vesc();
    inline decltype(VescUart::data)& currentData() override {return data;}
    inline bool queryData() override {return getVescValues();}
};
