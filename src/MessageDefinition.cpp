#include "MessageDefinition.h"

const MessageDefinition MessageDefinition::changePilot = MessageDefinition(
    changePilotID,
    {MQTT::IntercomNetworkMessage::Mode::Yes,
     MQTT::IntercomNetworkMessage::Mode::No,
     MQTT::IntercomNetworkMessage::Mode::Timeout},
    Symbol::changePilot_Symbol);

const MessageDefinition MessageDefinition::returnToStand = MessageDefinition(
    returnToStandID,
    {MQTT::IntercomNetworkMessage::Mode::Yes,
     MQTT::IntercomNetworkMessage::Mode::No,
     MQTT::IntercomNetworkMessage::Mode::Timeout},
    Symbol::returnToStand_Symbol);

const MessageDefinition MessageDefinition::faster = MessageDefinition(
    fasterID,
    {MQTT::IntercomNetworkMessage::Mode::ReceivedOk,
     MQTT::IntercomNetworkMessage::Mode::Timeout},
    Symbol::faster_Symbol);

const MessageDefinition MessageDefinition::lower = MessageDefinition(
    lowerID,
    {MQTT::IntercomNetworkMessage::Mode::ReceivedOk,
     MQTT::IntercomNetworkMessage::Mode::Timeout},
    Symbol::lower_Symbol);

const MessageDefinition MessageDefinition::iWantToComeBack = MessageDefinition(
    iWantToComeBackID,
    {MQTT::IntercomNetworkMessage::Mode::Yes,
     MQTT::IntercomNetworkMessage::Mode::No},
    Symbol::iWantToComeBack_Symbol);

const MessageDefinition MessageDefinition::technicalPb_Blocked = MessageDefinition(
    technicalPb_BlockedID,
    {MQTT::IntercomNetworkMessage::Mode::ReceivedOk},
    Symbol::technicalPb_Blocked_Symbol);

const MessageDefinition MessageDefinition::technicalPb_ComingBack = MessageDefinition(
    technicalPb_ComingBackID,
    {MQTT::IntercomNetworkMessage::Mode::ReceivedOk},
    Symbol::technicalPb_ComingBack_Symbol);

const std::map<MQTT::IntercomNetworkMessage::MessageID,MessageDefinition> MessageDefinition::database = {
    {changePilotID,changePilot},
    {returnToStandID,returnToStand},
    {fasterID,faster},
    {lowerID,lower},
    {iWantToComeBackID,iWantToComeBack},
    {technicalPb_BlockedID,technicalPb_Blocked},
    {technicalPb_ComingBackID,technicalPb_ComingBack}
};

const bool MessageDefinition::acceptedAnswerIfRequest(MQTT::IntercomNetworkMessage::Mode mode){
    for(MQTT::IntercomNetworkMessage::Mode m: possibleAnswers){
        if(m==mode){
            return true;
        }
    }
    return false;
}
