#pragma once

struct I_Button{
    virtual ~I_Button(){}//destructor
    virtual bool isPressed()=0;
};
