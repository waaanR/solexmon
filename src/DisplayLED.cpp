#include "DisplayLED.h"

constexpr auto MATRIX_SIZE = MATRIX_HEIGHT*MATRIX_WIDTH;
constexpr auto MATRIX_GPIO = 19;

DisplayLED::DisplayLED(int ledBrightness):LED_BRIGHTNESS(ledBrightness){
    FastLED.addLeds<WS2812, MATRIX_GPIO, GRB>((CRGB*)matrix.colors, MATRIX_SIZE);
    pinMode(MATRIX_GPIO, OUTPUT);
    resetMatrix();
}

void DisplayLED::display(){
    FastLED.setBrightness(LED_BRIGHTNESS);
    FastLED.show();
}

void DisplayLED::resetMatrix(){
    for(int i=0;i<MATRIX_HEIGHT;i++){
        for(int j=0;j<MATRIX_WIDTH;j++){
            matrix.colors[i][j] = CRGB::Black;
        }
    }
    display();
}

void DisplayLED::setMatrix(Symbol symbol){
    for(int i=0;i<MATRIX_HEIGHT;i++){
        for(int j=0;j<MATRIX_WIDTH;j++){
            switch (symbol.bitmap.lines[i][j]) {
            case Symbol::PixelState::EMP:
                matrix.colors[i][j] = CRGB::Black;
                break;
            default:
                switch(symbol.color){
                case Symbol::Color::None:
                    matrix.colors[i][j] = CRGB::Black;
                    break;
                case Symbol::Color::Purple:
                    matrix.colors[i][j] = CRGB::Purple;
                    break;
                case Symbol::Color::Blue:
                    matrix.colors[i][j] = CRGB::Blue;
                    break;
                case Symbol::Color::Cyan:
                    matrix.colors[i][j] = CRGB::Cyan;
                    break;
                case Symbol::Color::Green:
                    matrix.colors[i][j] = CRGB::Green;
                    break;
                case Symbol::Color::Yellow:
                    matrix.colors[i][j] = CRGB::Yellow;
                    break;
                case Symbol::Color::Orange:
                    matrix.colors[i][j] = CRGB::Orange;
                    break;
                case Symbol::Color::Red:
                    matrix.colors[i][j] = CRGB::Red;
                    break;
                case Symbol::Color::White:
                    matrix.colors[i][j] = CRGB::White;
                    break;
                }
            }
        }
    }
}

void DisplayLED::drawMessage(Symbol symbol){
    setMatrix(symbol);
    display();
}

void DisplayLED::drawERROR(){
    drawMessage(Symbol::error_Symbol);
}
