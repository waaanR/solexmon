#pragma once
#include "I_Button.h"
#include "stdint.h"

class Button;

/*
    Screen_Button constructor configurations:
    (number = id to give to the constructor to use the corresponding button)
    -------------------------
    |                       |
    |                       |
    |                       |
    |                       |
    |                       |
    |                       |
    -------------------------
    |   o       o       o   |
    -------------------------

    Config 1: (3 Buttons)
    -------------------------
    |                       |
    |                       |
    |                       |
    |                       |
    |                       |
    |                       |
    -------------------------
    |   1   |   2   |   3   |
    -------------------------

    Config 2: (5 Buttons)
    -------------------------
    |                       |
    |                       |
    |          8            |
    |                       |
    |                       |
    |-----------------------|
    -------------------------
    | 4 |   5   |   6   | 7 |
    -------------------------
*/

//! a button for using the M5
struct Screen_Button: public I_Button{
    Screen_Button(uint8_t idButton);
    ~Screen_Button();
    bool isPressed() override;
    static void initializeHardware();
private:
    Button* button;
};
