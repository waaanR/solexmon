#include "MessageDialog.h"

//! Number of seconds to display a indication before the timeout
constexpr auto TimeoutIndication = 10;
//! Number of seconds to display a request before the timeout
constexpr auto TimeoutRequest = 20;
//! Number of seconds to display a notification (if the answer has been sent or not) before the timeout
constexpr auto TimeoutNotificationMessageSentOrNot = 1.5;
//! Number of seconds to be able to send a new message after that an other one has just been sent
constexpr auto CoolDownBeforeSendAgain = 5;


DialogDisplay::DialogDisplay(I_Display& display, std::unique_ptr<I_Button> y ,std::unique_ptr<I_Button> n)
    :display(display)
    ,displayedSymbol(Symbol::reset_Symbol)
    ,flashedSymbol(Symbol::reset_Symbol)
{
    YesBt = std::move(y);
    NoBt = std::move(n);
}

void DialogDisplay::message(const Symbol& flashedSymbol, const Symbol& displayedSymbol, uint64_t timeout, uint64_t flashStartTime, State state){
    frameDisplayedCount=0;
    this->flashedSymbol=flashedSymbol;
    this->displayedSymbol=displayedSymbol;
    this->timeout=timeout;
    this->flashStartTime=flashStartTime;
    this->state=state;
    display.drawMessage(displayedSymbol);
}

void DialogDisplay::tick(){
    frameDisplayedCount++;

    if(frameDisplayedCount>=timeout*LoopFrequency){
        display.drawMessage(Symbol::reset_Symbol);
        if(state==State::DisplayingNotificationMsgSentOrNot){
            state=State::Inactive;
        } else{
            state=State::TimedOut;
        }
        return;
    }else if(frameDisplayedCount>=flashStartTime*LoopFrequency){
        if((frameDisplayedCount%30)<15){
            if(!flashBlackState){
                display.drawMessage(flashedSymbol);
                flashBlackState=true;
            }
        }else{
            if(flashBlackState){
                display.drawMessage(displayedSymbol);
                flashBlackState=false;
            }
        }
    }

    if(frameDisplayedCount>2*LoopFrequency){
        if (state==State::DisplayingIndication){
            if(YesBt->isPressed() || NoBt->isPressed()){
                state=State::TimedOut;
            }
        } else if (state==State::DisplayingRequest){
            if(YesBt->isPressed()){
                state=State::Yes;
                flashBlackState=false;
            }else if(NoBt->isPressed()){
                state=State::No;
                flashBlackState=false;
            }
        }
    }
}

MessageDialog::MessageDialog(I_Display &display,
                             MQTT &mqtt,
                             std::unique_ptr<I_Button> yesBt_,
                             std::unique_ptr<I_Button> noBt_,
                             std::unique_ptr<I_Button> wantToComeBackBt_,
                             std::unique_ptr<I_Button> tchPbBlockedBt_,
                             std::unique_ptr<I_Button> tchPbComingBackBt_)
    :dialogDisplay(display,std::move(yesBt_),std::move(noBt_))
    ,mqtt(mqtt)
{
    queue = std::queue<MQTT::IntercomNetworkMessage>();
    wantToComeBackBt = std::move(wantToComeBackBt_);
    tchPbBlockedBt = std::move(tchPbBlockedBt_);
    tchPbComingBackBt = std::move(tchPbComingBackBt_);
}

void MessageDialog::handleMessage(MQTT::IntercomNetworkMessage &mqttMessage){
    if(mqttMessage.modality==MQTT::IntercomNetworkMessage::Mode::MalFormed || !MessageDefinition::database.count(mqttMessage.messageId)) return;
    queue.push(mqttMessage);
    if(dialogDisplay.status()==DialogDisplay::State::DisplayingRequest || dialogDisplay.status()==DialogDisplay::State::DisplayingIndication){
        reset(0);
    }
}

void MessageDialog::reset(int setCoolDown){
    queue.pop();
    dialogDisplay.reset();
    coolDown = setCoolDown;
    dialogDisplay.display.drawMessage(Symbol::reset_Symbol);
}

bool MessageDialog::tick(){
    auto sendMsg=[this](MQTT::IntercomNetworkMessage& msg){
        if(mqtt.sendMessage(msg)){
            dialogDisplay.message(
                Symbol::reset_Symbol,
                Symbol::send_Message_Ok_Symbol,
                TimeoutNotificationMessageSentOrNot,
                TimeoutNotificationMessageSentOrNot,
                DialogDisplay::State::DisplayingNotificationMsgSentOrNot);
        } else{
            dialogDisplay.message(
                Symbol::reset_Symbol,
                Symbol::send_Message_Not_Ok_Symbol,
                TimeoutNotificationMessageSentOrNot,
                TimeoutNotificationMessageSentOrNot,
                DialogDisplay::State::DisplayingNotificationMsgSentOrNot);
        }
    };

    if(queue.empty()){
        if(coolDown==0){
            if(dialogDisplay.status()==DialogDisplay::State::Inactive){
                MQTT::IntercomNetworkMessage::MessageID id="";
                if(wantToComeBackBt->isPressed()){id = iWantToComeBackID;}
                if(tchPbBlockedBt->isPressed()){id = technicalPb_BlockedID;}
                if(tchPbComingBackBt->isPressed()){id = technicalPb_ComingBackID;}

                if(!id.empty()){
                    MQTT::IntercomNetworkMessage mqttMessage = MQTT::IntercomNetworkMessage(
                        this->mqtt.SOLEX_ID,
                        id,
                        MQTT::IntercomNetworkMessage::Mode::Request
                        );
                    sendMsg(mqttMessage);
                    coolDown = CoolDownBeforeSendAgain*LoopFrequency;
                }
            }
        }else{
            coolDown--;
        }
    } else {
        MQTT::IntercomNetworkMessage& mqttMessage = queue.front();
        MessageDefinition currentMessage = MessageDefinition::database.find(mqttMessage.messageId)->second;

        switch(mqttMessage.modality){

        case MQTT::IntercomNetworkMessage::Mode::Timeout: /*if we receive a timed out msg we resend it*/
            mqttMessage.modality = MQTT::IntercomNetworkMessage::Mode::Request;
            mqtt.sendMessage(mqttMessage);
            reset(0);
            break;


        case MQTT::IntercomNetworkMessage::Mode::Yes:
        case MQTT::IntercomNetworkMessage::Mode::ReceivedOk:
            if(dialogDisplay.status()==DialogDisplay::State::Inactive){
                dialogDisplay.message(
                    Symbol::yes_Symbol,
                    currentMessage.symbol,
                    TimeoutIndication,
                    0,
                    DialogDisplay::State::DisplayingIndication);
            } else if(dialogDisplay.status()==DialogDisplay::State::TimedOut){
                reset(CoolDownBeforeSendAgain*LoopFrequency);
            }
            break;
        case MQTT::IntercomNetworkMessage::Mode::No:
            if(dialogDisplay.status()==DialogDisplay::State::Inactive){
                dialogDisplay.message(
                    Symbol::no_Symbol,
                    currentMessage.symbol,
                    TimeoutIndication,
                    0,
                    DialogDisplay::State::DisplayingIndication);
            } else if(dialogDisplay.status()==DialogDisplay::State::TimedOut){
                reset(CoolDownBeforeSendAgain*LoopFrequency);
            }
            break;


        case MQTT::IntercomNetworkMessage::Mode::Request:
            if(dialogDisplay.status()==DialogDisplay::State::Inactive){
                if(currentMessage.acceptedAnswerIfRequest(MQTT::IntercomNetworkMessage::Mode::Yes)
                    && currentMessage.acceptedAnswerIfRequest(MQTT::IntercomNetworkMessage::Mode::No)
                    && currentMessage.acceptedAnswerIfRequest(MQTT::IntercomNetworkMessage::Mode::Timeout)){ // if the message want a Yes/No answer
                    dialogDisplay.message(
                        Symbol::reset_Symbol,
                        currentMessage.symbol,
                        TimeoutRequest,
                        TimeoutRequest/2,
                        DialogDisplay::State::DisplayingRequest);
                }else{
                    dialogDisplay.message(
                        Symbol::reset_Symbol,
                        currentMessage.symbol,
                        TimeoutRequest/2,
                        TimeoutRequest/2,
                        DialogDisplay::State::DisplayingRequest);
                }
            }

            if(currentMessage.acceptedAnswerIfRequest(MQTT::IntercomNetworkMessage::Mode::Yes)
                && dialogDisplay.status()==DialogDisplay::State::Yes){

                mqttMessage.modality = MQTT::IntercomNetworkMessage::Mode::Yes;
                sendMsg(mqttMessage);
                queue.pop();
                coolDown = CoolDownBeforeSendAgain*LoopFrequency;

            }else if(currentMessage.acceptedAnswerIfRequest(MQTT::IntercomNetworkMessage::Mode::No)
                       && dialogDisplay.status()==DialogDisplay::State::No){

                mqttMessage.modality = MQTT::IntercomNetworkMessage::Mode::No;
                sendMsg(mqttMessage);
                queue.pop();
                coolDown = CoolDownBeforeSendAgain*LoopFrequency;


            }else if(currentMessage.acceptedAnswerIfRequest(MQTT::IntercomNetworkMessage::Mode::Timeout)
                       && dialogDisplay.status()==DialogDisplay::State::TimedOut){

                mqttMessage.modality = MQTT::IntercomNetworkMessage::Mode::Timeout;
                mqtt.sendMessage(mqttMessage);
                reset(0);


            }else if(currentMessage.acceptedAnswerIfRequest(MQTT::IntercomNetworkMessage::Mode::ReceivedOk)
                       && (dialogDisplay.status()==DialogDisplay::State::Yes||dialogDisplay.status()==DialogDisplay::State::No)){

                mqttMessage.modality = MQTT::IntercomNetworkMessage::Mode::ReceivedOk;
                sendMsg(mqttMessage);
                queue.pop();
                coolDown = CoolDownBeforeSendAgain*LoopFrequency;
            }
            break;

        default: break;
        }
    }

    if(dialogDisplay.status()==DialogDisplay::State::DisplayingRequest
        || dialogDisplay.status()==DialogDisplay::State::DisplayingIndication
        || dialogDisplay.status()==DialogDisplay::State::DisplayingNotificationMsgSentOrNot){
        dialogDisplay.tick();
    }

    return dialogDisplay.status()!=DialogDisplay::State::DisplayingRequest
           && dialogDisplay.status()!=DialogDisplay::State::DisplayingIndication
           && dialogDisplay.status()!=DialogDisplay::State::DisplayingNotificationMsgSentOrNot;
}
