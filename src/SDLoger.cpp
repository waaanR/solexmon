#include "SDLoger.h"

#ifndef MOCK_SD

bool SDLogger::sdInited=false;

SDLogger::SDLogger(String fileName):fileName(fileName){
    if(!sdInited){
        while (!SD.begin(GPIO_NUM_4, SPI, 25000000)){
            delay(10);
        }
        sdInited=true;
    }
    newFile();
}

void SDLogger::newFile(){
    if(current){
        current.flush();
        current.close();
    }
    for (int c=0;;c++) {
        auto fn=String("/")+fileName+String(c)+".txt";
        if(SD.exists(fn)) continue;

        current=SD.open(fn,FILE_WRITE,true);
        break;
    }
}

SDLogger::File &SDLogger::f(){return *(File*)&current;/*just a cast to a more desirable interface*/}

SDLogger::File &SDLogger::File::operator<<(SDLogger::Endl){
    fs::File::println("");
    return *this;
}

SDLogger::File& SDLogger::File::operator<<(GPS::Pos& pos){
    auto& self=*this;
    self << pos.lon;
    self << pos.lat;
    return self;
}

SDLogger::File &SDLogger::File::operator<<(decltype(VescUart::data) &data){
    auto& self=*this;
    self << data.avgMotorCurrent;
    self << data.avgInputCurrent;
    self << data.dutyCycleNow;
    self << data.rpm;
    self << data.inpVoltage;
    self << data.ampHours;
    self << data.ampHoursCharged;
    self << data.wattHours;
    self << data.wattHoursCharged;
    self << data.tachometer;
    self << data.tachometerAbs;
    self << data.tempMosfet;
    self << data.tempMotor;
    self << data.pidPos;
    self << data.id;
    self << data.error;
    return self;
}

void SDLogger::File::writeTimeHeader(){
    auto& self=*this;
    self << "TimeStamp" << "date";
}

void SDLogger::File::writeGpsHeader(){
    auto& self=*this;
    self << "Lon" << "Lat";
}

void SDLogger::File::writeVescHeader(){
    auto& self=*this;
    self << "avgMotorCurrent";
    self << "avgInputCurrent";
    self << "dutyCycleNow";
    self << "rpm";
    self << "inpVoltage";
    self << "ampHours";
    self << "ampHoursCharged";
    self << "wattHours";
    self << "wattHoursCharged";
    self << "tachometer";
    self << "tachometerAbs";
    self << "tempMosfet";
    self << "tempMotor";
    self << "pidPos";
    self << "id";
    self << "error";
    self << endl;
}

#endif
