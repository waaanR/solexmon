#pragma once

#include "I_GPS.h"
#include <vector>
#include <TinyGPS++.h>

struct GPS: public I_GPS{
    inline GPS(){ gpsStream.begin(9600);}
    void synch() override;
    //! -1 => No GPS detected: check wiring.
    inline uint32_t satCount() override {return satCount_; }
    inline Pos& currentPos() override { return pos; }
private:
    uint32_t satCount_=0;//!< -1 => No GPS detected: check wiring.
    Pos pos;
    TinyGPSPlus gps;
    HardwareSerial gpsStream=HardwareSerial(2);
};
