#pragma once

#include "I_Vuart.h"

struct Mock_Vesc: public I_Vesc{
    inline Mock_Vesc(){
        data.tempMotor=data.tempMosfet=data.inpVoltage=45;
    }
    inline decltype(VescUart::data)& currentData() override {return data;}
    inline bool queryData() override {
        data.inpVoltage-=0.02;
        if(data.inpVoltage<45)data.inpVoltage=60;
        data.tempMotor=data.tempMosfet=data.inpVoltage;
        return true;
    }
private:
    decltype(VescUart::data) data;
};
