#include "GPIO_Button.h"

GPIO_Button::GPIO_Button(uint8_t gpio)
    :gpio(gpio){
    pinMode(this->gpio, INPUT_PULLUP);
}
bool GPIO_Button::isPressed(){
    return !digitalRead(gpio);
}
