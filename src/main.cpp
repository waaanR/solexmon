#include <Arduino.h>
#include <sstream>
#include <list>
#include <map>
#include "DisplayLED.h"
#include "MessageDialog.h"
#include "Mock_GPS.h"
#include "Mock_Vuart.h"
#include "mqtt.h"
#include "Gps.h"
#include "SDLoger.h"
#include "Vuart.h"
#include "DisplayM5.h"
#include "Screen_Button.h"
#include "GPIO_Button.h"

void setup(){}

struct Cadenser{
    Cadenser(float frequency/*Hz*/):dt(1000000/frequency){}
    void tick(){
        int64_t current;
        while (true){
            current=esp_timer_get_time();
            if(current>nextTarget) break;
        }
        nextTarget=current+dt;
    }
private:
    int64_t nextTarget=0;//!< in microseconds
    int64_t dt;//!< in microseconds
};

struct Counter{
    Counter(uint64_t tickInterval,std::function<void()> f):tickInterval(tickInterval),f(f){}
    void tick(){
        current++;
        if(current>=tickInterval){
            current=0;
            f();
        }
    }
private:
    uint64_t tickInterval;
    std::function<void()> f;
    uint64_t current=0;
};

struct M5Stack_Init: public M5GFX{
    M5Stack_Init(){
        begin();
        setRotation(2);
    }

    void info(std::string s){
        constexpr auto S=30;
        if(s.length()>S){
            info(s.substr(0,S));
            info(s.substr(S,s.length()));
            return;
        }

        if(infoBuffer.size()>20){
            infoBuffer.pop_front();
        }
        infoBuffer.push_back(s);
        if (!displayBusy()){
            startWrite();{
                fillScreen(TFT_BLACK);
                setTextColor(TFT_WHITE);
                setTextDatum(textdatum_t::top_left);
                setFont(&fonts::DejaVu12);

                uint8_t lineIndex=0;
                for (auto& infoLine : infoBuffer) {
                    drawString(infoLine.c_str(),0,17*lineIndex++);
                }
            }endWrite();
        }
    }

private:
    std::list<std::string> infoBuffer;
};



void loop(){

    M5Stack_Init m5StackInit;//initialize the M5 device to be able to deal with the SD card and display init infos

#ifdef MOCK_SD
    m5StackInit.info("/!\\ MOCK SD /!\\");
#else
    m5StackInit.info("SD Card init...");
#endif

    SDLogger logger("VescLog");
    logger.f().writeTimeHeader();
    logger.f().writeGpsHeader();
    logger.f().writeVescHeader();

    SDReader reader;

#ifndef MOCK_SD
    while(!reader.configOk){
        reader.getConfig();
    }
#endif

    if(reader.m5Version!=1 && reader.m5Version!=2){
        m5StackInit.info("Unknown m5Version");
        while(true){}
    }
    if(strcmp(reader.buttons.c_str(), "Touch") && strcmp(reader.buttons.c_str(), "Press")){
        m5StackInit.info("Unknown buttons");
        while(true){}
    }
    if(strcmp(reader.messageDisplay.c_str(), "DisplayM5") && strcmp(reader.messageDisplay.c_str(), "DisplayLED")){
        m5StackInit.info("Unknown message display");
        while(true){}
    }

    m5StackInit.info("-- Config chosen --");
    m5StackInit.info("M5Core" + std::to_string(reader.m5Version));
    m5StackInit.info(reader.mock_GPS?"/!\\ GPS mock /!\\":"GPS real");
    m5StackInit.info(reader.mock_UART?"/!\\ UART mock /!\\":"UART real");
    m5StackInit.info("Message display: " + reader.messageDisplay);
    if(!strcmp(reader.messageDisplay.c_str(), "DisplayLED")){
        m5StackInit.info(" | ledBrightness: " + std::to_string(reader.ledBrightnessIfLedUsed));
    }
    m5StackInit.info("Buttons: " + reader.buttons);
    if(!strcmp(reader.buttons.c_str(), "Press")){
        m5StackInit.info(" | buttons GPIO:");
        m5StackInit.info("  | yes: " + std::to_string(reader.yesBt));
        m5StackInit.info("  | no: " + std::to_string(reader.noBt));
        m5StackInit.info("  | come back: " + std::to_string(reader.wantToComeBackBt));
        m5StackInit.info("  | blocked: " + std::to_string(reader.blockedBt));
        m5StackInit.info("  | pb: " + std::to_string(reader.tchPbComingBackBt));
    }
    m5StackInit.info("Wifi ssid: " + reader.wifi_ssid);
    m5StackInit.info("SolexID: " + reader.solexId);
    m5StackInit.info("------------------------");
    delay(15000);

    if(reader.m5Version == 2){
        Screen_Button::initializeHardware();// this must be called before DisplayM5 initialisation to use screen_buttons
    }

    DisplayM5 displayM5(reader.lowThresholdVoltage,
                        reader.veryLowThresholdVoltage,
                        reader.highThresholdTempMotor,
                        reader.veryHighThresholdTempMotor,
                        reader.highThresholdTempMosfet,
                        reader.veryHighThresholdTempMosfet);

    displayM5.info("Message display init...");
    I_Display* messageDisplay;
    if(!strcmp(reader.messageDisplay.c_str(), "DisplayLED")){
        DisplayLED displayLED(reader.ledBrightnessIfLedUsed);
        messageDisplay = &displayLED;
    } else{
        messageDisplay = &displayM5;
    }

    displayM5.info("Wifi & MQTT init...");
    MQTT mqtt(reader.solexId, reader.wifi_ssid, reader.wifi_pass);

    displayM5.info("GPS init...");
    I_GPS* gps;
    reader.mock_GPS?gps=new Mock_GPS():gps=new GPS();

    displayM5.info("Vesc init...");
    I_Vesc* vesc;
    reader.mock_UART?vesc=new Mock_Vesc():vesc=new Vesc();

    displayM5.info("Dialog init...");
    MessageDialog* dialog;
    if(!strcmp(reader.buttons.c_str(), "Touch")){
        if(reader.m5Version == 2){//use buttons of M5Core2: cf. Screen_Button.h
            dialog = new MessageDialog(*messageDisplay,
                                       mqtt,
                                       std::unique_ptr<Screen_Button>(new Screen_Button(4)),
                                       std::unique_ptr<Screen_Button>(new Screen_Button(5)),
                                       std::unique_ptr<Screen_Button>(new Screen_Button(6)),
                                       std::unique_ptr<Screen_Button>(new Screen_Button(7)),
                                       std::unique_ptr<Screen_Button>(new Screen_Button(8)));

        } else if(reader.m5Version == 1){//use buttons of M5Core1: 37=top button, 38=mid button, 39=bottom button
            dialog = new MessageDialog(*messageDisplay,
                                       mqtt,
                                       std::unique_ptr<GPIO_Button>(new GPIO_Button(37)),
                                       std::unique_ptr<GPIO_Button>(new GPIO_Button(39)),
                                       std::unique_ptr<GPIO_Button>(new GPIO_Button(37)),
                                       std::unique_ptr<GPIO_Button>(new GPIO_Button(38)),
                                       std::unique_ptr<GPIO_Button>(new GPIO_Button(39)));
        }
    } else if(!strcmp(reader.buttons.c_str(), "Press")){//use real buttons on the solex handlebar
        dialog = new MessageDialog(*messageDisplay,
                                   mqtt,
                                   std::unique_ptr<GPIO_Button>(new GPIO_Button(reader.yesBt)),
                                   std::unique_ptr<GPIO_Button>(new GPIO_Button(reader.noBt)),
                                   std::unique_ptr<GPIO_Button>(new GPIO_Button(reader.wantToComeBackBt)),
                                   std::unique_ptr<GPIO_Button>(new GPIO_Button(reader.blockedBt)),
                                   std::unique_ptr<GPIO_Button>(new GPIO_Button(reader.tchPbComingBackBt)));
    }

    //subscribe a topic in the tryconnect() function of mqtt (therefore in the constructor and against logout)
    mqtt.mqtt_stage4A.setCallback([&](char* topic, uint8_t* buffer, unsigned int bufferSize){
        if(!strcmp(topic,mqtt.TOPIC_MQTT_INPUT_SOLEX_MESSAGE.c_str())){
            MQTT::IntercomNetworkMessage mqttMessage = MQTT::IntercomNetworkMessage::fromJsonBuffer(buffer,bufferSize);
            if(!strcmp(mqttMessage.solexId.c_str(),mqtt.SOLEX_ID.c_str())){
                dialog->handleMessage(mqttMessage);
            }
        }
    });

    Counter updateDisplay(2*LoopFrequency/*2sec*/,[&](){
        displayM5.drawData(vesc->currentData(),mqtt.ok(),mqtt.getDate(),gps->satCount());
        logger.f().flush();
    });

    Counter updateMQTT(1*LoopFrequency/*1sec*/,[&](){
        MQTT::Data data;{
            data.latitude=gps->currentPos().lat;
            data.longitude=gps->currentPos().lon;
            data.avgMotorCurrent=vesc->currentData().avgMotorCurrent;
            data.avgInputCurrent=vesc->currentData().avgInputCurrent;
            data.dutyCycleNow=vesc->currentData().dutyCycleNow;
            data.rpm=vesc->currentData().rpm;
            data.inpVoltage=vesc->currentData().inpVoltage;
            data.ampHours=vesc->currentData().ampHours;
            data.ampHoursCharged=vesc->currentData().ampHoursCharged;
            data.wattHours=vesc->currentData().wattHours;
            data.wattHoursCharged=vesc->currentData().wattHoursCharged;
            data.tempMosfet=vesc->currentData().tempMosfet;
            data.tempMotor=vesc->currentData().tempMotor;
            data.pidPos=vesc->currentData().pidPos;
            data.tachometer=vesc->currentData().tachometer;
            data.tachometerAbs=vesc->currentData().tachometerAbs;
        }
        mqtt.sendTelemetry(data);
    });

    Counter renewLogFile(10*60*LoopFrequency/*10min*/,[&](){
        logger.newFile();
        logger.f().writeTimeHeader();
        logger.f().writeGpsHeader();
        logger.f().writeVescHeader();
    });

    displayM5.info("start loop");
    Cadenser cadenser(LoopFrequency);

    bool displayData = true;//!< true if data can be displayed, false if the display is occupied by the dialog

    while (true) {
        cadenser.tick();
        gps->synch();

        if (vesc->queryData()) {
            logger.f() << esp_timer_get_time() << mqtt.getDate().c_str() << gps->currentPos() << vesc->currentData() << SDLogger::endl;
            if(displayData)updateDisplay.tick();
            renewLogFile.tick();
            updateMQTT.tick();
            displayData = dialog->tick();
        }else{
            displayM5.drawERROR();
        }
    }
}
